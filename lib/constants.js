// Covid Notice
export const COVID_NOTICE = {
    'title': 'We are open for business',
    'url': '/covid-notice/',
    'date': 'May 2021'
}


// SendGrid Config


// Newsletter Config
export const MAILCHIMP_LIST_ID = 'list-id'
export const MAILCHIMP_API = 'mc-api-key'
export const MAILCHIMP_SUBSCRIBE_URL = 'https://mailchi.mp/c57dffe0d8da/newsletter-signup'


// Site Config
export const SITE_NAME = 'Plumbers Hampshire';
export const SITE_TITLE_TEMPLATE = `%s - ${SITE_NAME}`;
export const SITE_URL = 'https://plumbers-hampshire.com'