// Next Dependencies
import Head from 'next/head'
import { useRouter } from 'next/router'
import ErrorPage from 'next/error'

// Components
import Container from '../../components/container'
import MoreStories from '../../components/more-stories'
import { Layout } from '../../components'
import PostHeader from '../../components/Post/post-header'
import PostBody from '../../components/Post/post-body'
import PostTitle from '../../components/Post/post-title'

// Sections
import { Blog, CallToAction } from '../../sections';

// GraphQL and Data
import website from '../../website'
import { MAILCHIMP_SUBSCRIBE_URL } from '../../lib/constants'
import { getAllPostsWithSlug, getPostAndMorePosts } from '../../lib/api'


export default function PostPage({ post, posts, preview }) {
  const router = useRouter()
  const morePosts = posts?.edges

  if (!router.isFallback && !post?.slug) {
    return <ErrorPage statusCode={404} />
  }

  return (
    <Layout preview={preview}>
      <Head>
        <title>{post.title} - {website.name}</title>
        <meta property="og:image" content={post.featuredImage?.node?.sourceUrl} />
      </Head>

      <section className="py-20">
        <Container>
          {router.isFallback ? (
            <PostTitle>Loading…</PostTitle>
          ) : (
            <article>
              <PostHeader title={post.title} />
              
              <PostBody date={post.date}>
                <PostTitle />
                
                
              </PostBody>
            </article>

            <Blog posts={morePosts} />
          )}
        </Container>
      </section>

      <CallToAction
        heading="Subscribe To Our Newsletter"
        introText="Get our blog articles sent to your inbox each month and keep up to date with recent changes."
        backgroundUrl="/call-to-action/newsletter-subscribe.png"
        buttonText="Subscribe Today"
        buttonLink={MAILCHIMP_SUBSCRIBE_URL}
      />
    </Layout>
  )
}

export async function getStaticProps({ params, preview = false, previewData }) {
  const data = await getPostAndMorePosts(params.slug, preview, previewData)

  return {
    props: {
      preview,
      post: data.post,
      posts: data.posts,
    },
  }
}

export async function getStaticPaths() {
  const allPosts = await getAllPostsWithSlug()

  return {
    paths: allPosts.edges.map(({ node }) => `/blog/${node.slug}`) || [],
    fallback: false,
  }
}
