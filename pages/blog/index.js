// next dependencies
import Head from 'next/head';
import Link from 'next/link';

// Components
import { Layout } from '../../components';

// Sections
import { CallToAction, Blog } from '../../sections';

// Data
import {SITE_NAME} from '../../lib/constants';
import { getAllPostsWithSlug } from '../../lib/api';


export default function Index({ allPosts: { edges }, preview }) {
  const allPosts = edges

  return (
    <Layout preview={preview}>
      <Head>
        <title>Blog - {SITE_NAME}</title>
      </Head>

      <h2>Blog</h2>
      <p>This is the blog index page content ... </p>
      { /* <Blog posts={allPosts} /> */ }

      <aside className="sidebar">
        <div className="sidebar__widget widget__text">
          <h5>Search</h5>

          <form>
            <input type="text" className="input search" placeholder="Search Website..." />
            <button type="button" className="btn btn-primary">Search</button>
          </form>
        </div>

        <div className="sidebar__widget widget__popular_posts">
          <h5>Popular Posts</h5>
          
          <nav className="menu">
            <ul>
              <li>
                <Link href=""><a>
                  <img src="http://wp.plumbers-hampshire.ubuntu.local/wp-content/uploads/2021/02/white-radiator-appartment-scaled-e1612966132102.jpg" />
                  Can You Get Finance For Boiler Upgrades and Replacements?
                </a></Link>
              </li>

              <li>
                <Link href=""><a>
                  <img src="http://wp.plumbers-hampshire.ubuntu.local/wp-content/uploads/2021/04/modern-turbofan-boiler-1.jpg" />
                  Benefits & Drawbacks of Residential LPG Boilers
                </a></Link>
              </li>

              <li>
                <Link href=""><a>
                  <img src="http://wp.plumbers-hampshire.ubuntu.local/wp-content/uploads/2021/03/smart-meter-household-display.jpg" />
                  Buyers Guide – Smart Thermostat Upgrades
                </a></Link>
              </li>

              <li>
                <Link href=""><a>
                  <img src="http://wp.plumbers-hampshire.ubuntu.local/wp-content/uploads/2021/03/plumber-kitchen-pipe-repair-1.jpg" />
                  What To Consider When Choosing An Emergency Plumber
                </a></Link>
              </li>
            </ul>
          </nav>
        </div>

        <div className="sidebar__widget widget__categories">
          <h5>Search</h5>

          <nav className="menu">
            <ul>
              <li><Link href="/blog/category/bathroom-renovations/"><a>Bathroom Renovations</a></Link></li>
              <li><Link href="/blog/category/buyers-guides/"><a>Buyers Guides</a></Link></li>
              <li><Link href="/blog/category/heating-advice/"><a>Heating Advice</a></Link></li>
              <li><Link href="/blog/category/plumbing-advice/"><a>Plumbing Advice</a></Link></li>
              <li><Link href="/blog/category/drainage-advice/"><a>Drainage Advice</a></Link></li>
              <li><Link href="/blog/category/energy-saving/"><a>Energy Saving</a></Link></li>              
              <li><Link href="/blog/category/landlords-advice/"><a>Landlords Advice</a></Link></li>              
            </ul>
          </nav>
        </div>

      </aside>
    </Layout>
  
  )
}

export async function getStaticProps({ preview = false }) {
  const allPosts = await getAllPostsWithSlug(preview)

  return {
    props: { allPosts, preview },
  }
}
