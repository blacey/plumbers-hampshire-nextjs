// next dependencies
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'

// Components
import Container from '../components/container'
import { MediaText } from '../components'
import { Layout } from '../components'

// Sections
import { CallToAction } from '../sections'

// GraphQL Queries and Data
import website from '../config/website.json'


export default function Index({ preview }) {
  return (    
    <Layout preview={preview}>
      <Head>
        <title>Plumbing, Heating &amp; Drainage Solutions in Hampshire - {website.name}</title>
      </Head>

      <CallToAction
        heading="Plumbing, Heating & Drainage Experts"
        introText="We help residential and commercial customers within Hampshire to fix problems, help with upgrades and offer expert advice when required."
        backgroundUrl="https://res.cloudinary.com/dvu9u7egp/image/upload/v1620192864/plumber-man-fixing-kitchen-sink-1.jpg"
        buttonText="Get Free Quote"
        buttonLink="/contact/"
      />

      <section className="py-20">
        <Container>
          <h1 className="text-5xl mb-8 font-normal text-center py-20 tracking-tight">Plumbing, Heating and Drainage Specialists in Hampshire</h1>
          
          <MediaText className="mb-4">
            <div className="order-1 flex md:w-1/2 sm:w-auto">
              <Image src="https://res.cloudinary.com/dvu9u7egp/image/upload/v1620466198/modern-house-bathroom-interior.jpg" alt="Modern Bathroom Interior" width="620" height="600" className="w-full h-full" />
            </div>

            <div className="order-2 flex md:w-1/2 sm:w-auto items-center justify-center flex-col p-10">
              <h2 className="text-2xl mb-4 font-normal w-full tracking-tight">Plumbing Services</h2>
              <p className="leading-relaxed w-full">Our Plumbers in Hampshire have some tools and materials in their vans to solve most plumbing issues during the first visit. If we cannot resolve the issue, then our plumbing engineer will provide you with a detailed estimate and time scale for remedial work. We provide reliable plumbing services, including installations, upgrades, boiler repairs and maintenance work for clients across Hampshire.</p>
              
              <div className="button-group align-items-start flex w-full">
                <Link to="/services/residential/plumbing/" href="/services/residential/plumbing/"><a className="btn btn-primary btn-md py-2 px-5 mr-5 bg-yellow-500 hover:bg-yellow-500 rounded-md text-black text-left">Residential Plumbing</a></Link>
                <Link to="/services/commercial/plumbing/" href="/services/commercial/plumbing/"><a className="btn btn-secondary btn-md py-2 px-5 bg-blue-600 hover:bg-blue-500 rounded-md text-white text-left">Commercial Plumbing</a></Link>
              </div>
            </div>
          </MediaText>

          <MediaText>
            <div className="order-2 flex md:w-1/2 sm:w-auto">
              <Image src="https://res.cloudinary.com/dvu9u7egp/image/upload/v1620466213/repair-gas-boiler-3.jpg" alt="Heating Engineer Repairing Gas Boiler" width="620" height="600" className="w-full h-full" />
            </div>

            <div className="order-1 flex md:w-1/2 sm:w-auto items-center justify-center flex-col p-10">
              <h2 className="text-2xl mb-4 font-normal w-full tracking-tight">Heating Services</h2>
              <p className="leading-relaxed w-full">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In interdum scelerisque elit ut facilisis. Vivamus magna ex, volutpat ac pharetra nec, cursus in dolor. Mauris vehicula est at nunc ultricies facilisis. Nulla imperdiet eget metus sit amet viverra. Proin sem sapien, efficitur non mollis eu, auctor et justo. Sed tellus nisi, faucibus vitae malesuada sed, egestas sed magna. Proin a malesuada nisl, vel ullamcorper sapien.</p>
              
              <div className="button-group align-items-start flex w-full">
                <Link to="/services/residential/heating/" href="/services/residential/heating/"><a className="btn btn-primary btn-md py-2 px-5 mr-5 bg-yellow-500 hover:bg-yellow-700 rounded-md text-black text-left">Residential Plumbing</a></Link>
                <Link to="/services/commercial/heating/" href="/services/commercial/heating/"><a className="btn btn-secondary btn-md py-2 px-5 bg-blue-600 hover:bg-blue-500 rounded-md text-white text-left">Commercial Plumbing</a></Link>
              </div>
            </div>
          </MediaText>

          <MediaText className="mb-4">
            <div className="order-1 flex md:w-1/2 sm:w-auto">
              <Image src="https://res.cloudinary.com/dvu9u7egp/image/upload/v1620290159/drainage-shutterstock.jpg" alt="Drainage Services" width="620" height="600" className="w-full h-full" />
            </div>

            <div className="order-2 flex md:w-1/2 sm:w-auto items-center justify-center flex-col p-10">
              <h2 className="text-2xl mb-4 font-normal w-full tracking-tight">Drainage Services</h2>
              <p className="leading-relaxed w-full">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In interdum scelerisque elit ut facilisis. Vivamus magna ex, volutpat ac pharetra nec, cursus in dolor. Mauris vehicula est at nunc ultricies facilisis. Nulla imperdiet eget metus sit amet viverra. Proin sem sapien, efficitur non mollis eu, auctor et justo. Sed tellus nisi, faucibus vitae malesuada sed, egestas sed magna. Proin a malesuada nisl, vel ullamcorper sapien.</p>
              
              <div className="button-group align-items-start flex w-full pt-5">
                <Link to="/services/residential/drainage/" href="/services/residential/drainage/"><a className="btn btn-md py-2 px-5 mr-5 bg-yellow-500 hover:bg-yellow-500 rounded-md text-black-500 text-left">Residential Plumbing</a></Link>
                <Link to="/services/commercial/drainage/" href="/services/commercial/drainage/"><a className="btn btn-md py-2 px-5 bg-blue-600 hover:bg-blue-500 rounded-md text-white text-left">Commercial Plumbing</a></Link>
              </div>
            </div>
          </MediaText>

        </Container>
      </section>
      
      <CallToAction
        heading="Are You Ready To Improve Your Home?"
        introText="We can help renovate your premises and help to fix any problem you are experiencing. Our team have years of experience and industry know-how to get the job done right first time."
        backgroundUrl="https://res.cloudinary.com/dvu9u7egp/image/upload/v1620466198/modern-house-bathroom-interior.jpg"
        buttonText="Get Free Quote"
        buttonLink="/contact/"
      />

    </Layout>  
  )
}