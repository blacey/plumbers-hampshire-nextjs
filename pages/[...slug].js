// next dependencies
import Head from 'next/head'
import {useRouter} from 'next/router'
import ErrorPage from 'next/error'

// Components
import { Layout } from '../components'
import Container from '../components/container'
import PostBody from '../components/Post/post-body'

// Sections
import { CallToAction } from '../sections'

// Data
import website from '../config/website.json'


export default function Page({ posts, preview }) {
  const router = useRouter()
  const page = posts;

  if (!router.isFallback && !page?.slug) {
    return <ErrorPage statusCode={404} />
  }

  return (
    <Layout preview={preview}>
      <Head>
        <title>{page.title} - {website.name}</title>
        <meta property="og:image" content={page.featuredImage?.node?.sourceUrl} />
      </Head>

      <CallToAction
        heading={page.title}
        introText="Get our blog articles sent to your inbox each month and keep up to date with recent changes."
        backgroundUrl={page.featuredImage?.node?.sourceUrl}
        buttonText="Get Free Quote"
      />

      <section className="py-20">
        <Container>
          {router.isFallback ? (
            <PostTitle>Loading…</PostTitle>
          ) : (
            
            <article className="my-20">
              <PostBody date={page.date}>
                {page.content}
              </PostBody>
            </article>
          
          )}
        </Container>
      </section>

      <CallToAction
        heading="Improve Your Home Today"
        introText="Contact our team to see how we can help transform your home or business premises."
        backgroundUrl="/call-to-action/cover1.jpg"
        buttonText="Contact Us"
        buttonLink="/contact/"
      />
    </Layout>
  )
}