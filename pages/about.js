// next dependencies
import Head from 'next/head';
import Link from 'next/link';
import { NextSeo } from 'next-seo';

// Components
import Container from '../components/container';
import CoverImage from '../components';
import { Layout } from '../components';

// Sections
import { CallToAction } from '../sections';

// Data
import website from '../config/website.json';


export default function About({ preview }) {

  return (    
    <Layout preview={preview}>
      <Head>
        <title>About Us - {`${website.name}`}</title>
      </Head>

      <CallToAction
        heading="About Us"
        introText="We want to make it easier for customers to access professional plumbing, heating and drainage services at home and in commercial settings."
        backgroundUrl="/cover-images/about.jpg"
        buttonText="Get Free Quote"
        buttonLink="/contact/"
      />

      <section className="py-20">
        <Container>
          <article className="my-20">

            <h1 className="text-3xl mb-8 font-normal text-center">About {`${website.name}`}</h1>
            <p>Our professional team are here to help you with installations, upgrades, repairs and maintenance all year round.</p>
            <p>We have a <Link href="/blog/">company blog</Link> where we aim to help customers diagnose common faults. Our team will provide their experienced insights and knowledge on a regular basis, answering questions we receive.</p>

            <h2 className="text-2xl mt-5 mb-3">Why Choose Plumbers Hampshire?</h2>
            <p><strong>Local Plumbing Experts</strong> - We deliver high quality boiler repairs, installations and upgrades performed at properties throughout Hampshire.</p>
            <p><strong>Friendly and Professional Services</strong> - carried out by a fully-qualified team of bathroom and kitchen fitters who are CRB checked.</p>
            <p><strong>Installations, Maintenance and Upgrades~</strong> - Superior installations, central heating servicing and boiler repairs for brand new bathrooms and kitchens, which meet current building regulations in the UK.</p>
            <p><strong>Reliable Manufacturers</strong> - We use products from well known including Worcester, Bosh and Mitsubishi for all installations that are conducted by our team. If you are trying to find a Hampshire plumbing expert then look no further.</p>

          </article>
        </Container>
      </section>

      <CallToAction
        heading="Are You Ready To Improve Your Home?"
        introText="We can help renovate your premises and help to fix any problem you are experiencing. Our team have years of experience and industry know-how to get the job done right first time."
        backgroundUrl="/call-to-action/cover1.jpg"
        buttonText="Get Free Quote"
        buttonLink="/contact/"
      />
    </Layout>  
  )
}