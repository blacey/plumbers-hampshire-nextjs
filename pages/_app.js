// Next Dependencies
import { useState } from 'react'
import styled from 'styled-components'
import { DefaultSeo, LocalBusinessJsonLd } from 'next-seo'

// Vendor Dependencies
import { ApolloProvider } from '@apollo/client'
import client from '../lib/apollo/client'

// Components
import { Modal } from '../components'

// GraphQL Queries and Data
import website from '../config/website.json'

// Styles
import '../styles/index.css'

const ModalContainer = styled.div``

export default function MyApp({ Component, pageProps }) {
  { /* Modal States*/}
  const [showModal, setShowModal] = useState(false);
  const openModal = () => { setShowModal(prev => !prev) }

  return (
    <ApolloProvider client={client}>
      <DefaultSeo
        title={website.name}
        titleTemplate={`%s - ${website.name}`}
        description={website.description}
      />

      <Component {...pageProps} />

      <ModalContainer></ModalContainer>

      <LocalBusinessJsonLd
        type="Plumber"
        id={website.url}
        name={website.name}
        description={website.description}
        logo={website.logo.url}
        url={website.url}
        telephone={website.phone}
        address={{
          streetAddress: website.address.street,
          addressLocality: website.address.town,
          addressRegion: website.address.county,
          postalCode: website.address.postcode,
          addressCountry: website.address.county,
        }}

        geo={{
          latitude: website.address.latitude,
          longitude: website.address.longitude,
        }}

        /*
        images={website.images.map(image => {
          image
        })}

        sameAs={website.social.map(social => {
          social
        })}
        
        openingHours={website.openingHours.map(hours => {
          hours
        })}
        
        makesOffer={website.offers.map(offer => {
          priceSpecification: {
            type: "UnitPriceSpecification",
            priceCurrency: offer.priceCurrency,
            price: offer.price,
          },
          itemOffered: {
            name: offer.itemOffered.name,
            description: offer.itemOffered.description,
          }
        })}
        
        areaServed={website.areaServed.map(areaServed => {
          areaServed
        })}
        */
      />
    </ApolloProvider>
  )
}