// next dependencies
import Head from 'next/head'
import Link from 'next/link'
import { NextSeo } from 'next-seo'
import { GoogleMapReact, LocationPin } from 'react-google-maps'

// Components
import Container from '../components/container'
import { Layout } from '../components'

// Sections
import { CallToAction } from '../sections'

// Data
import website from '../config/website.json'


export default function Contact({ preview }) {
  const lat = website.address.latitude
  const lng = website.address.longitude

  return (    
    <Layout preview={preview}>
      <Head>
        <title>Contact {`${website.name}`}</title>
      </Head>

      <section className="bg-gray-200 w-full h-screen">
        <div>Map</div>
      </section>

      <section className="py-20">
        <Container>
          <article className="my-20">

            <h1 className="text-3xl mb-10 font-normal text-center">Contact {`${website.name}`}</h1>

            <div className="flex gap-10">
              <div className="w-1/2 sm-w-1/2 xs:w-full">
                <htmlForm className="htmlForm flex gap-5 flex-wrap rounded-md bg-gray-200 p-8">
                  <p>
                    <label htmlFor="name">Full Name</label>
                    <input type="text" name="name" className="w-full" />
                  </p>

                  <p>
                    <label htmlFor="email">Email</label>
                    <input type="email" name="email" className="w-full" />
                  </p>

                  <p>
                    <label htmlFor="service">Service</label>
                    <select name="service" className="w-full">
                      <option id="residential-plumbing">Residential Plumbing</option>
                      <option id="residential-heating">Residential Heating</option>
                      <option id="residential-drainage">Residential Drainage</option>
                      <option id="commercial-plumbing">Commercial Plumbing</option>
                      <option id="commercial-heating">Commercial Heating</option>
                      <option id="commercial-drainage">Commercial Drainage</option>
                    </select>
                  </p>
                </htmlForm>
              </div>

              <div className="w-1/2 sm-w-1/2 xs:w-full">
                <h2 className="text-xl mb-4 font-normal text-left">Contact Details</h2>
                
                <p>
                  <strong>Our Address: </strong><br/>
                  {website.address.street && <span>{website.address.street}, </span>}
                  {website.address.town && <span>{website.address.town}, </span>}
                  {website.address.county && <span>{website.address.county}, </span>}
                  {website.address.postcode && <span>{website.address.postcode}</span>}
                </p>
                
                {website.phone && <div><strong>Phone: </strong><br/> {website.phone}</div>}
                {website.email && <div><strong>Email: </strong><br/> {website.email}</div>}
                <div><strong>Opening Hours:</strong><br/> Mon - Fri 9am until 6pm<br/></div>
              
              </div>
            </div>

            </article>
        </Container>
      </section>

      <CallToAction
        heading="Are You Ready To Improve Your Home?"
        introText="We can help renovate your premises and help to fix any problem you are experiencing. Our team have years of experience and industry know-how to get the job done right first time."
        backgroundUrl="/call-to-action/cover1.jpg"
        buttonText="Get Free Quote"
        buttonLink="/contact/"
      />
    </Layout>  
  )
}