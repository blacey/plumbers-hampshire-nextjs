// next.config.js
module.exports = {
  trailingSlash: true,
  images: {
    domains: ['localhost', 'wphampshireplumberslocal.local', 'res.cloudinary.com'],
  },
}
