// Next Dependencies
import PostPreview from '../../components/post-preview'
import Container from '../../components/container'

// Data
import website from '../../config/website.json'
import { getLatestPosts } from '../../lib/api'

export async function getStaticProps({ preview = false }) {
  const latestPosts = await getLatestPosts(preview)

  return {
    props: { latestPosts, preview },
  }
}

export default function Blog({ latestPosts }) {
  return (
    <Container>
      <h4 className="text-3xl mb-10 font-normal text-white">Latest Articles</h4>

      <div className="flex flex-nowrap gap-5">
        {latestPosts?.edges?.map(({ node }) => (
          <PostPreview
            key={node.slug}
            title={node.title}
            coverImage={node.featuredImage?.node}
            date={node.date}
            slug={node.slug}
            categories={node.categories}
            classes={{
              "article": "bg-white w-full md:w-1/3 opacity-75",
              "date": "text-sm text-black-500 mb-0"
            }}
          />
        ))}
      </div>

      {website.services.newsletter.subscribeUrl && website.services.newsletter.subscribeUrl !== 'undefined'}
        <button href="{website.newsletter.subscribeUrl}" className="mt-10 btn btn-primary btn-lg py-4 px-8 px-5 bg-yellow-600 hover:bg-yellow-500 rounded-md text-white">Subscribe To Newsletter</button>
    </Container>  
  );
}
