import Image from 'next/image'
import Link from 'next/link'
import styled from 'styled-components'
import Container from '../../components/container'
import styles from './CallToAction.module.css'

export default function CallToAction({
  heading,
  introText,
  backgroundUrl,
  buttonText='Get Free Quote',
  buttonLink='/contact/',
  layout=''
}) {

  const CallToActionSection = styled.section`
    margin: 0px auto;
    position: relative;
    background-color: rgba(0, 0, 0, 0.6);
    
  `;

  const CallToActionContent = styled.div`    
    height:500px;
    padding: 100px 20px;
    margin: 0px auto;
    display:flex;
    align-items:center;
    color:#FFF;
  `;

  const CallToActionImage = styled.img`
    width:100%;
    height:500px;
    background-image: url(${backgroundUrl});
    background-size:cover;
    background-repeat:no-repeat;
    background-position:center center;
  `

  return (
    <>
      <CallToActionSection>
        <CallToActionContent className="flex items-center justify-center py-20">
          <div className="panel text-center">
            <h4 className="text-3xl mb-5 font-normal">{heading}</h4>
            <p className="text-1xl mb-5">{introText}</p>
            { buttonLink != '' ? (
              <button href={buttonLink} type="button" className="btn btn-primary btn-md py-2 px-5 bg-orange-600 hover:bg-orange-500 rounded-md"><a>{buttonText}</a></button>
            ) : '' }
          </div>
        </CallToActionContent>
      </CallToActionSection>
    </>
  )
}
