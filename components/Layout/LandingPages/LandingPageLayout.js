import { SiteMeta } from '../../components';
import { CallToAction } from '../../sections';

export default function Layout({ preview, children }) {
  return (
    <>
      <SiteMeta />

      <section className="min-h-screen">
        <main>{children}</main>
      </section>

      <CallToAction
        heading="Are You Ready To Improve Your Home?"
        introText="We can help renovate your premises and help to fix any problem you are experiencing. Our team have years of experience and industry know-how to get the job done right first time."
        backgroundUrl="http://wp.plumbers-hampshire.ubuntu.local/wp-content/uploads/2021/03/smart-meter-household-display.jpg"
        buttonText="Get Free Quote"
        buttonLink="/contact/"
      />

    </>
  )
}
