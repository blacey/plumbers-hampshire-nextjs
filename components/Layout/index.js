import { SiteMeta, SiteHeader, SiteFooter } from '../../components';

export default function Layout({ preview, children }) {
  return (
    <>
      <SiteMeta />
      
      <SiteHeader />

      <section className="min-h-screen">
        <main>{children}</main>
      </section>

      <SiteFooter />
    </>
  )
}
