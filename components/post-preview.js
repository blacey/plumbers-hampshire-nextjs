// Next Dependencies
import Link from 'next/link'
import Image from 'next/image'

// Vendor Dependedncies
import { BuildUrl } from 'cloudinary-build-url';

// Components
import Date from '../components/date'


export default function PostPreview({
  title,
  coverImage,
  date,
  excerpt,
  slug,
  classes
}) {
  return (
    <article className={`${classes.article} post max-w-50 p-5 rounded-md flex justify-center items-center overflow-hidden shadow-lg`}>
      <Link as={`/blog/${slug}`} href="/blog/[slug]">
        <a>
          {coverImage && coverImage.sourceUrl !== undefined && (
            <div className="mb-5">
              <Image 
                src={coverImage?.sourceUrl}
                alt={title}
                className="rounded-md w-full"
                width="480"
                height="300"
              />
            </div>
          )}

          <h4 className="text-lg mb-3">
            {title}
          </h4>

          <div className={`${classes.date}`}>
            <Date dateString={date} />
          </div>
        </a>
      </Link>
    </article>
  )
}
