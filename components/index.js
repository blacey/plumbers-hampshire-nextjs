export { default as SiteMeta } from './Meta/'
export { default as SiteHeader } from './SiteHeader/'
export { default as SiteFooter } from './SiteFooter/'

export { default as Modal } from './Modal/'
// export { default as Contact } from './Contact/Contact'
export { default as NewsletterForm } from './Newsletter/Newsletter'

export { default as Menu } from './Menu/'
export { default as CoverImage } from './CoverImage/'
export { default as MediaText } from './MediaText/'

// Layouts (Home, Pages, SEO Location, Blog)
export { default as Layout } from './Layout/'