// Next Dependencies
import styled from 'styled-components'
import { MdClose } from 'react-icons/md'

// Components
import { NewsletterForm } from '../../components'

const Background = styled.div`
    background:rgba(0, 0, 0, 0.8);
    width:100vw;
    height:100vh;
    position:fixed;
    display:flex;
    justify-content:center;
    align-items:center;
`

const ModalContainer = styled.div`
    width:800px;
    height:500px;
    box-shadow:0 5px 16px rgba(0, 0, 0, 0.2);
    background:#FFF;
    color:#333;
    display:grid;
    grid-template-columns:1fr 1fr;
    position:relative;
    z-index:500;
    border-radius:10px;
`

const ModalImage = styled.img`
    width:100%;
    height:100%;
    border-radius:10px 0 0 10px;
    background:#000;
`

const ModalContent = styled.div`
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
    line-height:1.8;
    color:#444;
    padding:20px;

    p {
        margin-bottom:1.5rem;
    }

    form {
        padding:30px 0;
    }
`

const CloseModalButton = styled(MdClose)`
    cursor:pointer;
    position:absolute;
    top:20px;
    right:20px;
    width:32px;
    padding:0;
    z-index:10;
`


export default function Modal({showModal, setShowModal, children}) {
    return (
        <>
        {showModal ? (
            <Background>
                <ModalContainer showModal={showModal}>
                    <ModalImage src={modalData.image.src} alt={modalData.image.alt} />

                    <ModalContent>
                        <heading className="text-2xl font-normal mb-5 text-center">{modalData.title}</heading>
                        <div>{children}</div>
                    </ModalContent>

                    <CloseModalButton aria-label="Close Modal" onClick={() => setShowModal(prev => !prev)} />
                </ModalContainer>
            </Background>
        ) : null}
        </>
    )
}
