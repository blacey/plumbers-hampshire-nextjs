// Next Dependencies
import React from 'react'
import Image from 'next/image'
import styled from 'styled-components'

export default function MediaText({ className, children }) {  
    return (
        <>
            { children.length > 0 ? (
            <section className="media-text w-full bg-gray-200 my-20">
                <div className="flex">
                    {children}
                </div>
            </section>
            ) : null}
        </>
    )
}