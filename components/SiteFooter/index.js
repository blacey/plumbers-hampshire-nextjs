// Next Dependencies
import Link from 'next/link'

// Components
import Container from '../../components/container'
import { NewsletterForm } from '../../components'

// Sections
import { Blog } from '../../sections'

// Data
import website from '../../config/website.json'


export default function Footer({ footerPosts, preview }) {
  const YEAR = new Date().getFullYear();

  return (
    <>
      <section className="blog bg-gray-600 py-20 flex justify-center items-center text-center">
        <Blog />
      </section>

      <footer className="site-footer border-none">
        <Container>
          <div className="py-10 flex flex-col lg:flex-row items-top">
            <div className="widget text-left my-10 lg:mr-10 lg:w-1/3">
              <h5 className="text-lg mb-5 font-normal tracking-tight leading-tight">Gas Safe Specialists</h5>
              <p className="mb-5">The Gas Safe Engineers Register is an official list of gas engineers who work safely and legally. Do not let anyone touch your boiler if they are not Gas Safe approved specialist. It is important that you always check that the heating specialist is on the Gas Safe Register with an active license.</p>
              <p className="mb-5">Telephone: <a href="tel:08004085500">0800 408 5500</a></p>
              <a href="https://gassaferegister.co.uk/" className="btn btn-primary" target="_blank" rel="external nofollow">Read More</a>
            </div>

            <div className="widget text-top my-10 lg:mr-10 lg:w-1/3">
              <h5 className="text-lg mb-5 font-normal tracking-tight leading-tight">Newsletter Sign-Up</h5>
              <p className="form-intro pb-10 px-0">We will send you a monthly email with our latest articles - we won't send you any marketing emails.</p>
              <NewsletterForm />
            </div>

            <div className="widget text-top my-10 lg:w-1/3">
              <h5 className="text-lg mb-5 font-normal tracking-tight leading-tight">Important Links</h5>
              <nav className="footer-menu">
                <ul>
                  <li className="mb-4"><Link href="/privacy/">Privacy Policy</Link></li>
                  <li className="mb-4"><Link href="/cookies/">Cookie Policy</Link></li>
                  <li className="mb-0"><Link href="/contact/">Contact Us</Link></li>
                </ul>
              </nav>
            </div>
          </div>
        </Container>

        <Container>
          <div className="copyright text-center pt-3 pb-5">
            <p className="pb-2">Website &copy; {website.name} {YEAR}</p>
            <a href="https://laceytechsolutions.co.uk/" rel="external" target="_blank" className="text-orange-700">Plumbing Website</a> by Lacey Tech
          </div>
        </Container>  
      </footer>
    </>
  )
}