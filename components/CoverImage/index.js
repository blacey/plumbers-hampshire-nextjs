import Image from 'next/image';
import styles from './CoverImage.module.css';
import React, { useState } from 'react';
import { Modal } from '../../components';


export default function CoverImage({ title, intro, coverImage }) {
  

  const modalData = {
    'image': {
      'src': '/modals/cover2.jpg',
      'alt': 'Newsletter'
    },
    'title': 'Contact Us Today',
    'content': '<p>Subscribing to our newsletter is free and you can un-subscribe at any time.</p>',
    'form': 'newsletter'
  }

  return (

    <div className={styles.container}>

      {coverImage?.sourceUrl !== undefined && (
        <Image
          src={coverImage?.sourceUrl}
          className={styles.coverImage}
          alt={title}
          width="2000"
          height="500"
        />
      )}

      <div className="overlay">
        <div className="text-2xl font-normal tracking-tighter leading-tight mb-12 text-center">{title}</div>
        <p>{intro}</p>

        <button 
          type="button"
          className="btn btn-primary btn-md py-2 px-5 bg-orange-600 hover:bg-orange-500 rounded-md text-white"
        >Get Free Estimate</button>
      </div>

    </div>
  )
}
