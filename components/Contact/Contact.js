

export default function ContactForm() {
  return (
    <form className="form newsletter w-full max-w-lg">
      <div className="flex flex-wrap -mx-3 mb-6">

        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
            First Name
          </label>
          <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="John" />
        </div>

        <div className="w-full md:w-1/2 px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
            Last Name
          </label>
          <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="Doe" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-email">
            Email Address
          </label>
          <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-email" type="email" placeholder="john.doe@gmail.com" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-service">
            Service Required
          </label>
          <select className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-service">
            <option key="residential-plumbing" value="Residential Plumbing">Residential Plumbing</option>
            <option key="residential-heating" value="Residential Heating">Residential Heating</option>
            <option key="residential-plumbing" value="Residential Drainage">Residential Drainage</option>
            <option key="commercial-plumbing" value="Commercial Plumbing">Commercial Plumbing</option>
            <option key="commercial-heating" value="Commercial Heating">Commercial Heating</option>
            <option key="commercial-drainage" value="Commercial Drainage">Commercial Drainage</option>
          </select>
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-message">
            Message
          </label>
          <textarea className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-message" placeholder="How Can We Help You?"></textarea>
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-0">
        <div className="w-full px-3">
          <button className="flex-shrink-0 bg-purple-500 hover:bg-purple-700 border-purple-500 hover:border-pueple-700 text-sm border-4 text-white py-1 px-2 rounded" type="button">Sign Up</button>
        </div>
      </div>

    </form>
  )
}
