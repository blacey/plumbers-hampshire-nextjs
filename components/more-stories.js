import PostPreview from '../components/post-preview'

export default function MoreStories({ posts }) {
  return (
    <section className="py-20 background-gray-200">
      <h4 className="text-3xl text-center mb-10">Recent Articles</h4>

      <div className="grid grid-cols-1 md:grid-cols-2 md:col-gap-15 lg:col-gap-10 row-gap-10">
        {posts.map(({ node }) => (
          <PostPreview
            key={node.slug}
            title={node.title}
            coverImage={node.featuredImage?.node}
            excerpt={node.excerpt}
            date={node.date}
            slug={node.slug}
          />

	 /*
           <PostPreview
            key={node.slug}
            title={node.title}
            coverImage={node.featuredImage?.node}
            date={node.date}
            author={node.author?.node}
            slug={node.slug}
            excerpt={node.excerpt}
          />
         */
        ))}
      </div>
    </section>
  )
}
