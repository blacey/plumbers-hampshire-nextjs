// Next Dependencies
import { useState } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { LogoJsonLd } from 'next-seo'

// Vendor Dependencies
import { BuildUrl } from 'cloudinary-build-url'
import { FaBars, FaSearch, FaPhone } from 'react-icons/fa'

// Site Components
import Container from '../../components/container'
import { Menu } from '../../components'

// GraphQL Queries and Data
import website from '../../config/website.json'
import { GET_MENUS } from '../../lib/queries/get-menus';

export default function Header() {
  { /* Mobile Menu States*/ }
  const [ isMenuVisible, setMenuVisibility ] = useState( false );
  const headerMenu = GET_MENUS;

  return (
    <>
      <header className="site-header py-3 bg-white relative">
        <Container>

          <div className="flex flex-row justify-between items-center">
            <div className="header-left align-items-start min-w-200">
              <Link href="/">
                <a className="logo block">
                  <Image
                    src={website.logo.url}
                    alt={website.name}
                    width={website.logo.width}
                    height={website.logo.height}
                  />
                </a>
              </Link>
            </div>

            <div className="header-right flex">
              { website.features.search === true && (
                <button onClick={() => setMenuVisibility( ! isMenuVisible )} className="searchToggle flex-auto h-10 px-3 py-2 rounded text-black-300 border-black-300 hover:text-black-500 hover:border-black-500">
                  <FaSearch size="22px" />
                </button>
              )}

              { website.phone != '' && (
                <a
                  href={`tel:${website.phone}`}
                  className="h-10 inline-block flex-auto px-3 py-2 rounded text-black-300 border-black-300 hover:text-black-500 hover:border-black-500"
                >
                  <FaPhone size="22px" />
                </a>
              )}

              <button onClick={() => setMenuVisibility( ! isMenuVisible )} className="menuToggle h-10 flex-auto px-3 py-2 rounded text-black-300 border-black-300 hover:text-black-500 hover:border-black-500">
                <FaBars size="22px" />
              </button>
            </div>
          </div>

        </Container>

      </header>

      <div className={`${isMenuVisible ? 'visible' : 'hidden'}`}>
        <Menu menuItems={headerMenu?.edges} classes="overflow-hidden absolute md:relative top-16 left-0 md:top-0 z-20 flex flex-col font-semibold w-full md:w-auto md:rounded-none"/>
      </div>
    </>
  )
}
