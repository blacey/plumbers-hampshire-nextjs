import styles from './post-body.module.css'

export default function PostBody({ children }) {
  return (
      <div className="max-w mx-auto">
        {children}
      </div>
  )
}
