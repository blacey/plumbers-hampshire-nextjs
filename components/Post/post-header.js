import { CoverImage } from '../../components';

export default function PostHeader({
  title,
  coverImage,
  author,
}) {

  return (
    <header className="post-header mb-10 mx-0">
      <CoverImage title={title} coverImage={coverImage} author={author} />
    </header>
  )

}
