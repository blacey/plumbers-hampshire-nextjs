import { MAILCHIMP_LIST_ID, MAILCHIMP_API_KEY } from '../../lib/constants'

export default function NewsletterSubscribeForm() {
  return (
    <form className="form newsletter w-full max-w-sm flex flex-wrap mb-0">
      
      <div className="w-full md:w-1/2 pr-2 pb-3">
        <label 
          htmlFor="grid-first-name"
          className="sr-only block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
          First Name
        </label>

        <input 
          id="grid-first-name" 
          type="text" 
          className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
          placeholder="John" 
        />
      </div>

      <div className="w-full md:w-1/2 pl-2 pb-3">
        <label 
          htmlFor="grid-last-name"
          className="sr-only block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
          Last Name
        </label>

        <input 
          id="grid-last-name" 
          type="text" 
          className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
          placeholder="Doe" 
        />
      </div>
    
      <div className="w-full pb-3">
        <label 
          htmlFor="grid-email"
          className="sr-only block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
          Email Address
        </label>

        <input 
          id="grid-email"
          type="email" 
          className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
          placeholder="john.doe@gmail.com" 
        />
      </div>
    
    
      <div className="w-full pb-0 mb-0">
        <button 
          type="button"
          className="flex-shrink-0 bg-orange-500 hover:bg-orange-700 text-md border-0 text-white py-1 px-2 rounded">
          Complete Sign-Up
        </button>
      </div>
      

    </form>
  )
}
