import {isEmpty} from 'lodash';
import Link from 'next/link';
import styles from './Menu.module.css';

export default function Menu({ menuItems, classes }) {
    if ( isEmpty( menuItems ) ) {
		//return null;
	}

    return (
        <nav className="w-full h-100vw bg-blue-400 transition-opacity duration-1000 ease-out">
            <ul className="w-full text-center">
                <li className="w-full no-underline block p-1 text-gray-800 text-xl hover:text-orange-500"><Link key="1" href="/"><a className="block">Home</a></Link></li>
                <li className="w-full no-underline block p-1 text-gray-800 text-xl hover:text-orange-500"><Link key="2" href="/about"><a className="block">About</a></Link></li>
                <li className="w-full no-underline block p-1 text-gray-800 text-xl hover:text-orange-500"><Link key="3" href="/contact"><a className="block">Contact</a></Link></li>

                {menuItems?.map( menuItem => (
                    <li className="ml-3">
                        <Link key={menuItem?.node?.id} href={menuItem?.node?.path}>
                            <a className="block mt-4 lg:inline-block lg:mt-0 text-black-300 mr-4">
                                {menuItem?.node?.label}
                            </a>
                        </Link>
                    </li>
                ))}
            </ul>
        </nav>
    );
}