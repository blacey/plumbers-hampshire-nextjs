/**
 * Plumbers Hampshire.
 *
 * @since 1.0.0
 * @package Plumbers Hampshire
 * @copyright 2021 Lacey Tech Solutions
 * @link https://laceytechsolutions.co.uk
 */

import classnames from 'classnames';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';

/**
 * Contact form.
 *
 * @since 1.0.0
 * @author Dom Webber <dom.webber@hotmail.com>
 */
function Contact() {
  const inputClassnames = [
    'appearance-none',
    'block',
    'w-full',
    'bg-gray-200',
    'text-gray-700',
    'border',
    'border-gray-400',
    'rounded',
    'py-3',
    'px-4',
    'leading-tight',
    'focus:outline-none',
    'focus:bg-white',
    'focus:border-gray-500',
  ];

  const submitCallback = (values, { setSubmitting }) => {
    setTimeout(() => {
      setSubmitting(false);
    }, 400);
  };

  return (
    <Formik
      validationSchema={() => yup.object.shape({
        first_name: yup.string().min(1).required(),
        last_name: yup.string().min(1).required(),
        email: yup.string().email().required(),
      })}
      onSubmit={submitCallback}
    >
      {({ isSubmitting }) => (
        <Form className="form contact w-full max-w-lg">
          <div className="flex flex-wrap mx-3 mb-6">
            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
              <Field
                type="text"
                name="first_name"
                placeholder="John"
                className={classnames(inputClassnames)}
              />
              <ErrorMessage name="first_name" />
            </div>

            <div className="w-full md:w-1/2 px-3">
              <Field
                type="text"
                name="last_name"
                placeholder="Doe"
                className={classnames(inputClassnames)}
              />
              <ErrorMessage name="last_name" />
            </div>
          </div>

          <div className="flex flex-wrap mx-3 mb-6">
            <div className="w-full px-3">
              <Field
                type="email"
                name="email"
                placeholder="john.doe@example.com"
                className={classnames(inputClassnames)}
              />
              <ErrorMessage name="email" />
            </div>
          </div>

          <div className="flex flex-wrap mx-3 mb-0">
            <div className="w-full px-3">
              <button
                type="button"
                disabled={isSubmitting}
                className={
                  classnames([
                    'flex-shrink-0',
                    'bg-purple-500',
                    'hover:bg-purple-700',
                    'border-purple-500',
                    'hover:border-purple-700',
                    'text-sm',
                    'border-4',
                    'text-white',
                    'py-1',
                    'px-2',
                    'rounded',
                  ])
                }
              >
                Submit
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  )
}

export default Contact;
