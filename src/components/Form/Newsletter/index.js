/**
 * Plumbers Hampshire.
 *
 * @since 1.0.0
 * @package Plumbers Hampshire
 * @copyright 2021 Lacey Tech Solutions
 * @link https://laceytechsolutions.co.uk
 */

import classnames from 'classnames';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';

/**
 * Newsletter subscription form.
 *
 * @since 1.0.0
 * @author Dom Webber <dom.webber@hotmail.com>
 */
function Newsletter() {
  const inputClassnames = [
    'appearance-none',
    'block',
    'w-full',
    'bg-gray-200',
    'text-gray-700',
    'border',
    'border-gray-400',
    'rounded',
    'py-3',
    'px-4',
    'leading-tight',
    'focus:outline-none',
    'focus:bg-white',
    'focus:border-gray-500',
  ];

  const submitCallback = (values, { setSubmitting }) => {
    setTimeout(() => {
      setSubmitting(false);
    }, 400);
  }

  return (
    <Formik
      validationSchema={() => yup.object.shape({
        first_name: yup.string().min(1).required(),
        last_name: yup.string().min(1).required(),
        email: yup.string().email().required(),
      })}
      onSubmit={submitCallback}
    >
      {({ isSubmitting }) => (
        <Form classname="form newsletter w-full max-w-sm flex flex-wrap mb-0">
          <div className="w-full md:w-1/2 pr-2 pb-3">
            <Field
              type="text"
              name="first_name"
              placeholder="John"
              className={classnames(inputClassnames)}
            />
            <ErrorMessage name="first_name" />
          </div>

          <div className="w-full md:w-1/2 pl-2 pb-3">
            <Field
              type="text"
              name="last_name"
              placeholder="Doe"
              className={classnames(inputClassnames)}
            />
            <ErrorMessage name="last_name" />
          </div>

          <div className="w-full pb-3">
            <Field
              type="email"
              name="email"
              placeholder="john.doe@example.com"
              className={classnames(inputClassnames)}
            />
            <ErrorMessage name="email" />
          </div>

          <div className="w-full pb-0 mb-0">
            <button
              type="submit"
              className={
                classnames([
                  'flex-shrink-0',
                  'bg-orange-500',
                  'hover:bg-orange-700',
                  'text-md',
                  'border-0',
                  'text-white',
                  'py-1',
                  'px-2',
                  'rounded',
                ])
              }
            >
              Sign Up
            </button>
          </div>
        </Form>
      )}
    </Formik>
  )
}

export default Newsletter;
