/**
 * Plumbers Hampshire.
 *
 * @since 1.0.0
 * @package Plumbers Hampshire
 * @copyright 2021 Lacey Tech Solutions
 * @link https://laceytechsolutions.co.uk
 */

import React from 'react';
import { BreadcrumbJsonLd } from 'next-seo';
import { useRouter } from 'next/router';
import Link from 'next/link';

/**
 * Breadcrumbs component.
 * This builds the structure and styling for page breadcrumbs and
 * also constructs the JSON-LD schema markup accordingly. This
 * component is built on NextJS internals.
 *
 * @since 1.0.0
 * @author Dom Webber <dom.webber@hotmail.com>
 */
function Breadcrumb({ children, parts }) {
  const router = useRouter();
  const itemListElements = [];

  // Split the route by parts (/)
  let routeParts = router.pathname.split('/');

  // Exclude the part after a trailing slash
  routeParts = routeParts(part => part !== '');

  // Determine the title for the current page (non-linked)
  let current = routeParts[routeParts.length - 1];
  if (!children) {
    // Switch route part to Sentence case (punctuation ignored)
    current = current.charAt(0).toUpperCase() + current.substr(1).toLowerCase();
  } else {
    current = children;
  }

  return (
    <nav aria-label="Breadcrumb" className="py-3 px-3 md:px-4 lg:px-5">
      <ol>
        {
          routeParts.length > 0 && (
            <li key="home-base-page" className="breadcrumb-item">
              {/* This is the root URL */}
              <Link href="/">Home</Link>
            </li>
          )
        }

        {
          routeParts.map(
            (routePart, i) => {
              // Determine whether the current route part is the current page
              const isCurrent = (i + 1) === routeParts.length;

              // Construct the full href for the current breadcrumb item
              const partHref = `/${routeParts.slice(0, i - 1)?.join('/')}`;

              // Build a readable route name from the current breadcrumb item path
              let routePartReadable = routePart.charAt(0).toUpperCase();
              routePartReadable += routePart.substr(1).toLowerCase();

              // Determine whether a custom title for the breadcrumb item was provided
              const routeLinkText = parts !== undefined
              && parts[i] !== undefined
                ? parts[i]
                : routePartReadable;

              // Append the breadcrumb item to the JSON-LD list
              itemListElements.push({
                position: i + 1,
                name: isCurrent ? current : routeLinkText,
                item: partHref,
              });

              return (
                <li className="breadcrumb-item" aria-current={isCurrent ? 'page' : null} key={i}>
                  {
                    isCurrent ? (
                      current
                    ) : (
                      <Link href={partHref} passHref>routeLinkText</Link>
                    )
                  }
                </li>
              )
            }
          )
        }
      </ol>

      <BreadcrumbJsonLd
        itemListElements={itemListElements}
      />
    </nav>
  )
}